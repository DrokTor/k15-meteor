## Stack

I used Meteor 1.8.2 using --React for the front-end engine.

## How to

git pull URL dir  
cd dir  
meteor npm install   
meteor

To access the mongoDB instance and find the saved record, while meteor is still running, open another terminal window/tab on the same dir and run:  

meteor mongo  
db.registrations.find()  

## Libraries

For the UI: https://material-ui.com/  
String validation: https://www.npmjs.com/package/validator  
Password validation: https://www.npmjs.com/package/password-validator  
React recent Hooks API: https://reactjs.org/docs/hooks-intro.html  

### Dev Libraries

I like to work with eslint AirBnB es6 config.  
https://github.com/airbnb/javascript  

## Workflow

1# User enter the required info in the form.  
2# Submit.  
3# Validation, if there is an error, show error message.  
4# When no error, submit to the server using Meteor Call API.  
5# On the server, validate the form.  
6# If no error, insert into the DB and return  
7# If error in validation or in the insert, throw exception describing the error.  
8# The client receives the response, if no error, render success message.  
9# If error render error message according to the error description.  
