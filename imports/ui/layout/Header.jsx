import React from 'react';

const Header = () => (
  <header className="header">
    <img src="/logo_k15t.png" className="App-logo" width="120px" alt="logo" />
  </header>
);

export default Header;
