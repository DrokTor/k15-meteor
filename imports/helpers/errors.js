export const VALIDATION_ERROR = '[400 Bad Request]';
export const SERVER_ERROR = '[500 Internal Server Error]';
