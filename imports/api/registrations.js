import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import validate from '../helpers/validator';
import { VALIDATION_ERROR, SERVER_ERROR } from '../helpers/errors';

const Registration = new Mongo.Collection('registrations');

if (Meteor.isServer) {
  Meteor.methods({
    'registrations.insert': (form) => {
      const errState = {};
      // validate all fields before inserting into DB
      Object.keys(form).forEach((field) => {
        const confirm = field === 'confirmPassword' ? form.password : null;
        const err = validate(field, form[field], confirm);

        if (err) errState[`${field}Error`] = err;
      });

      if (Object.keys(errState).length > 0) {
        throw new Meteor.Error(VALIDATION_ERROR, 'Could not validate the form !', { ...errState });
      }

      try {
        return Registration.insert(form);
      } catch (e) {
        throw new Meteor.Error(SERVER_ERROR, 'Could not save registration !', 'Please try again or contact the support team for help.');
      }
    },
  });
}

export default Registration;
