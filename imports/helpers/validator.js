import isEmail from 'validator/lib/isEmail';
import equals from 'validator/lib/equals';
import isLength from 'validator/lib/isLength';
import isEmpty from 'validator/lib/isEmpty';
import PasswordValidator from 'password-validator';

const validate = (type, text, confirm) => {
  // granulate password validation to offer the user a precise feedback on the failed criteria
  const passwordLength = new PasswordValidator();
  passwordLength.is().min(8);

  const passwordUpper = new PasswordValidator();
  passwordUpper.has().uppercase();

  const passwordLower = new PasswordValidator();
  passwordLower.has().lowercase();

  const passwordDigits = new PasswordValidator();
  passwordDigits.has().digits();

  const passwordNoSpace = new PasswordValidator();
  passwordNoSpace.has().not().spaces();

  let error = false;

  if (type === 'name' && isEmpty(text)) {
    error = 'name cannot be empty.';
  } else if (type === 'email' && !isEmail(text)) {
    error = 'format not recongnizable.';
  } else if (type === 'password') {
    const checkLength = passwordLength.validate(text);
    const checkUpper = passwordUpper.validate(text);
    const checkLower = passwordLower.validate(text);
    const checkDigits = passwordDigits.validate(text);
    const checkNoSpace = passwordNoSpace.validate(text);

    if (!checkLength || !checkUpper || !checkLower || !checkDigits || !checkNoSpace) {
      const err = `
          ${!checkLength ? 'minimum 7 characters,' : ''}
          ${!checkUpper ? 'at least 1 uppercase, ' : ''}
          ${!checkLower ? 'at least 1 lowercase, ' : ''}
          ${!checkDigits ? 'at least 1 digit, ' : ''}
          ${!checkNoSpace ? 'no space, ' : ''}
          `;

      error = err;
    }
  } else if (type === 'confirmPassword' && !equals(text, confirm)) {
    error = 'password confirmation does not match.';
  } else if (type === 'address' && !isLength(text, { min: 20 })) {
    error = 'should be at least 20 characters.';
  }
  return error;
};

export default validate;
