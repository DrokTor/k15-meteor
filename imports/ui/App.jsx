import React from 'react';
import Header from './layout/Header';
import MainContent from './layout/MainContent';
import Footer from './layout/Footer';
import RegisterForm from './forms/RegisterForm';
import './App.css';

const App = () => (
  <div className="App">
    <Header />
    <MainContent>
      <RegisterForm />
    </MainContent>
    <Footer />
  </div>
);

export default App;
