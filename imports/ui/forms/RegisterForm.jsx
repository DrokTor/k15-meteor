import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Send from '@material-ui/icons/Send';
import Check from '@material-ui/icons/Check';
import ErrorOutline from '@material-ui/icons/ErrorOutline';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import { green, red } from '@material-ui/core/colors';
import validate from '../../helpers/validator';
import { VALIDATION_ERROR, SERVER_ERROR } from '../../helpers/errors';

const RegisterForm = () => {
  const [info, setInfo] = useState({ disabled: false, saved: false });
  const [form, setForm] = useState({
    name: 'dqsd',
    email: 'qsd@qsd.dd',
    password: 'Aaaaaaaaaaa1',
    confirmPassword: 'Aaaaaaaaaaa1',
    address: 'Aaaaaaaaaaa1 Aaaaaaaaaaa1 Aaaaaaaaaaa1',
    phone: '',
  });

  const [error, setError] = useState({
    nameError: '',
    emailError: '',
    passwordError: '',
    confirmPasswordError: '',
    addressError: '',
    phoneError: '',
  });

  // function to validate each field on change
  const validateField = (field, text, confirm) => {
    setError({ ...error, [`${field}Error`]: '' });
    const err = validate(field, text, confirm);
    if (err) setError({ ...error, [`${field}Error`]: err });
  };

  // function to submit the form
  // it uses Meteor.call API which has a callback returning result or error
  const submitForm = () => {
    const errState = {};
    Object.keys(form).forEach((field) => {
      setError({ ...error, [`${field}Error`]: '' });
      const confirm = field === 'confirmPassword' ? form.password : null;
      const err = validate(field, form[field], confirm);// confirm is only necessary for password

      if (err) errState[`${field}Error`] = err;
    });
    if (Object.keys(errState).length > 0) { // theres is at least 1 error
      setError({ ...errState });
    } else {
      setInfo({ ...info, disabled: true }); // freezing the form when submiting
      Meteor.call('registrations.insert', form, (err, res) => {
        if (res) setInfo({ saved: true }); // when successful
        if (err.error === SERVER_ERROR) {
          setInfo({ err });
        } else if (err.error === VALIDATION_ERROR) {
          setError({ ...err.details });
          setInfo({ disabled: false });
        }
      });
    }
  };

  return (
    <Grid container justify="center" spacing={0}>
      <Grid className="bannerText" container item md={12}>
        <h1>
          <span>Java Meetup Registration</span>
        </h1>
      </Grid>
      { // success message
        !info.err && info.saved && (
        <Grid container md={6} justify="center">
          <Check fontSize="large" style={{ marginTop: 40, color: green[500] }} />
          <Grid container item justify="center" md={12} style={{ marginTop: 20, marginBottom: 20 }}>
            <Typography variant="h5" gutterBottom>
             We successfully received your registration.
            </Typography>
          </Grid>
          <Grid container item justify="center" md={12}>
            <Typography variant="h4" gutterBottom>
              Thank you.
            </Typography>
          </Grid>
        </Grid>
        )
      }
      { // error message
        info.err && (
        <Grid container md={6} justify="center">
          <ErrorOutline fontSize="large" style={{ marginTop: 40, color: red[500] }} />
          <Grid container item justify="center" md={12} style={{ marginTop: 20, marginBottom: 20 }}>
            <Typography variant="h6" gutterBottom>
              { info.err.error }
            </Typography>
          </Grid>
          <Grid container item justify="center" md={12}>
            <Typography variant="subtitle1" gutterBottom>
              { info.err.reason }
            </Typography>
          </Grid>
          <Grid container item justify="center" md={12}>
            <Typography variant="subtitle1" gutterBottom>
              { info.err.details }
            </Typography>
          </Grid>
        </Grid>
        )
      }
      {
        !info.err && !info.saved && (
        <form>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <TextField
                required
                fullWidth
                disabled={info.disabled}
                label="Name"
                variant="outlined"
                error={!!error.nameError}
                helperText={error.nameError || ''}
                onBlur={(e) => validateField('name', e.target.value)}
                onChange={(e) => setForm({ ...form, name: e.target.value })}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                required
                fullWidth
                disabled={info.disabled}
                label="Email"
                variant="outlined"
                error={!!error.emailError}
                helperText={error.emailError || ''}
                onBlur={(e) => validateField('email', e.target.value)}
                onChange={(e) => {
                  validateField('email', e.target.value);
                  setForm({ ...form, email: e.target.value });
                }}
              />
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <TextField
                required
                fullWidth
                disabled={info.disabled}
                type="password"
                label="Password"
                variant="outlined"
                error={!!error.passwordError}
                helperText={error.passwordError || ''}
                onBlur={(e) => validateField('password', e.target.value)}
                onChange={(e) => {
                  validateField('password', e.target.value);
                  setForm({ ...form, password: e.target.value });
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                required
                fullWidth
                disabled={info.disabled}
                type="password"
                label="Confirm password"
                variant="outlined"
                error={!!error.confirmPasswordError}
                helperText={error.confirmPasswordError || ''}
                onBlur={(e) => validateField('confirmPassword', e.target.value, form.password)}
                onChange={(e) => {
                  validateField('confirmPassword', e.target.value, form.password);
                  setForm({ ...form, confirmPassword: e.target.value });
                }}
              />
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <TextField
                fullWidth
                disabled={info.disabled}
                label="Phone"
                variant="outlined"
                error={!!error.phoneError}
                helperText={error.phoneError || ''}
                onBlur={(e) => validateField('phone', e.target.value)}
                onChange={(e) => {
                  validateField('phone', e.target.value);
                  setForm({ ...form, phone: e.target.value });
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                required
                fullWidth
                disabled={info.disabled}
                label="Address"
                variant="outlined"
                error={!!error.addressError}
                helperText={error.addressError || ''}
                onBlur={(e) => validateField('address', e.target.value)}
                onChange={(e) => {
                  validateField('address', e.target.value);
                  setForm({ ...form, address: e.target.value });
                }}
              />
            </Grid>
          </Grid>
          <Button
            fullWidth
            variant="contained"
            style={{ marginTop: 25, padding: 15 }}
            color="primary"
            endIcon={info.disabled ? '' : <Send />}
            onClick={() => submitForm()}
          >
            { info.disabled ? <CircularProgress color="white" /> : 'Submit' }
          </Button>
        </form>
        )
      }

    </Grid>
  );
};

export default RegisterForm;
